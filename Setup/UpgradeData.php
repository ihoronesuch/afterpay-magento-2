<?php namespace Afterpay\Payment\Setup;

use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

class UpgradeData implements UpgradeDataInterface
{

    /**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @var \Magento\Eav\Setup\EavSetup $eavSetup
     */
    private $eavSetup;

    /**
     * Init
     *
     * @param EavSetup $eavSetup
     */
    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }


    /**
     * Upgrades data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $this->eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        if (!$this->eavSetup->getAttribute(\Magento\Customer\Model\Customer::ENTITY, 'cocnumber')) {
            $this->eavSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'cocnumber', [
                    'type'      => 'static',
                    'input'     => 'text',
                    'label'     => 'CoC number',
                    'required'  => false,
                    'system' => 0,
                ]);
        }

        $setup->endSetup();
    }
}