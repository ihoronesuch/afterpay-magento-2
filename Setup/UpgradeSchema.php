<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Afterpay\Payment\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{

    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '0.0.4', '<')) {
            $setup->getConnection()->addColumn(
                $setup->getTable('customer_entity'),
                'cocnumber',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'size' => 255,
                    'nullable' => true,
                    'default' => null,
                    'comment' => 'Customer CoC number'
                ]
            );

            $setup->getConnection()->addColumn(
                $setup->getTable('sales_order'),
                'afterpay_captured',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    'nullable' => false,
                    'default' => '0',
                    'comment' => 'Order captured in Afterpay'
                ]
            );
        }

        $setup->endSetup();
    }
}
