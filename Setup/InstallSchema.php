<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Afterpay\Payment\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Add AfterPay fee columns
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $connection = $installer->getConnection();
        $tables = [
            'sales_order',
            'sales_invoice',
            'quote',
            'sales_order_payment'
        ];

        foreach ($tables as $table) {
            $connection->addColumn(
                $installer->getTable($table),
                'afterpay_payment_fee',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                    'length' => '12,4',
                    'nullable' => true,
                    'comment' => 'AfterPay payment fee value'
                ]
            );

            $connection->addColumn(
                $installer->getTable($table),
                'base_afterpay_payment_fee',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                    'length' => '12,4',
                    'nullable' => true,
                    'comment' => 'AfterPay payment fee base value'
                ]
            );
        }
    }
}
