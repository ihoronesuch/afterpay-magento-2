<?php
/**
 * Copyright (c) 2016  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2016 arvato Finance B.V.
 */
namespace Afterpay\Payment\Block\Sales\Order;

use Afterpay\Afterpay;
use Magento\Framework\View\Element\Template;
use Afterpay\Payment\Model\Method\Factories;

/**
 * AfterPay service fee block for sales order
 */
class Fee extends Template
{
    /**
     * AfterPay payment methods factories
     *
     * @var Factories
     */
    protected $_paymentMethodFactories;

    /**
     * @var \Magento\Sales\Model\Order|null
     */
    protected $_order = null;

    /**
     * @var \Magento\Framework\DataObject
     */
    protected $_source;

    /**
     * @param Template\Context $context
     * @param Factories $paymentMethodFactories
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        Factories $paymentMethodFactories,
        array $data = []
    ) {
        $this->_paymentMethodFactories = $paymentMethodFactories;
        parent::__construct($context, $data);
    }

    /**
     * Get data (totals) source model
     *
     * @return \Magento\Framework\DataObject
     */
    public function getSource()
    {
        return $this->_source;
    }

    /**
     * Add payment fee to totals
     *
     * @return \Magento\Tax\Block\Sales\Order\Tax
     */
    public function initTotals()
    {
        $parent = $this->getParentBlock();
        $this->_order = $parent->getOrder();
        $this->_source = $parent->getSource();
        $paymentMethod = $this->_order->getPayment()->getMethod();

        if ($this->_paymentMethodFactories->paymentMethodExists($paymentMethod)
            && $this->_order->getAfterpayPaymentFee() != 0
        ) {
            $this->_addPaymentFee($paymentMethod);
        }

        return $this;
    }

    /**
     * Add payment fee string
     *
     * @param \Afterpay\Payment\Model\Method\DigitalInvoiceNL $paymentMethod
     * @param string $after
     * @return $this
     */
    protected function _addPaymentFee(
        $paymentMethod = '',
        $after = 'shipping'
    ) {
        $paymentMethodInstance = $this->_paymentMethodFactories->getPaymentMethodInstance($paymentMethod);
        $fee = new \Magento\Framework\DataObject(
            [
                'code' => $paymentMethodInstance->getFeeCode(),
                'value' => $this->_order->getAfterpayPaymentFee(),
                'label' => $paymentMethodInstance->getFeeTitle(),
            ]
        );

        $this->getParentBlock()->addTotal($fee, $after);
        return $this;
    }
}
