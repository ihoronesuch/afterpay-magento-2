<?php
/**
 * Copyright (c) 2016  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2016 arvato Finance B.V.
 */
namespace Afterpay\Payment\Helper\Service;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Stdlib\DateTime;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Config\Model\Config\Backend\Encrypted;
use Magento\Customer\Model\Session;
use Magento\Framework\ObjectManagerInterface;
use Magento\Payment\Helper\Data as PaymentHelper;
use Magento\Catalog\Model\Product as Product;

/**
 * AfterPay service helper
 */
class Data extends AbstractHelper
{
    /**
     * Order statuses
     */
    const ORDER_STATUS_PENDING = 'pending';

    const ORDER_STATUS_PROCESSING = 'processing';

    /**
     * Test mode label
     */
    const TEST_MODE_LABEL = 'Test mode, orders will not be fulfilled.';

    /**
     * XML Fields for configuration constants
     */
    const XML_FIELD_TEST_MODE_ACTIVE = 'testmode';

    const XML_FIELD_TEST_MODE_MERCHANT_ID = 'testmode_merchant_id';

    const XML_FIELD_TEST_MODE_PORTFOLIO_ID = 'testmode_portfolio_id';

    const XML_FIELD_TEST_MODE_PASSWORD = 'testmode_password';

    const XML_FIELD_TEST_MODE_PUSH_PASSWORD = 'testmode_push_password';

    const XML_FIELD_PROD_MODE_MERCHANT_ID = 'production_merchant_id';

    const XML_FIELD_PROD_MODE_PORTFOLIO_ID = 'production_portfolio_id';

    const XML_FIELD_PROD_MODE_PASSWORD = 'production_password';

    const XML_FIELD_PROD_MODE_PUSH_PASSWORD = 'production_push_password';

    const XML_FIELD_VAT_CONFIG_1 = 'vat_1';

    const XML_FIELD_VAT_CONFIG_2 = 'vat_2';

    const XML_FIELD_VAT_CONFIG_3 = 'vat_3';

    const XML_FIELD_VAT_CONFIG_4 = 'vat_4';

    const XML_FIELD_VAT_CONFIG_5 = 'vat_5';

    const XML_FIELD_VAT_CONFIG_FALLBACK = 'vat_fallback';

    const XML_FIELD_VAT_CONFIG_SHIPPING = 'vat_shipping';

    const XML_FIELD_VAT_CONFIG_DISCOUNT = 'vat_discount';

    const XML_FIELD_VAT_CONFIG_FEE = 'vat_fee';

    const XML_FIELD_PAYMENT_FEE_VALUE = 'fee_value';

    const XML_FIELD_PAYMENT_FEE_TITLE = 'fee_title';

    const XML_FIELD_REFUND_ENABLED = 'payment/afterpay_refund/active';

    const DEFAULT_PAYMENT_METHOD_MODEL = '\Afterpay\Payment\Model\Method\DigitalInvoiceNL';

    const PAYMENT_CAPTURE = 'payment/afterpay_capture/';
    /**
     * Iso languages for payment methods types
     *
     * @var $_isoLanguages
     */
    protected $_isoLanguages = [
        'afterpay_nl_digital_invoice' => 'NL',
        'afterpay_nl_direct_debit' => 'NL',
        'afterpay_be_digital_invoice' => 'NL',
        'afterpay_de_digital_invoice' => 'DE',
        'afterpay_nl_sepa_direct_debit' => 'NL',
        'afterpay_nl_business_2_business' => 'NL',
        'afterpay_nl_split_payments' => 'NL'
    ];

    /**
     * Logging
     *
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * Backend configurations
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * Payment Helper
     *
     * @var \Magento\Payment\Helper\Data
     */
    protected $_paymentHelper;

    /**
     * Format date
     *
     * @var DateTime
     */
    protected $_dateTime;

    /**
     * Decryptor
     *
     * @var Encrypted
     */
    protected $_encrypted;

    /**
     * Afterpay VAT categories mapping with Magento tax classes
     */
    protected $_VATSetup;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_appInterface;

    /**
     * @var \Magento\Catalog\Model\ProductManagement
     */
    protected $_productModel;

    /**
     * Constructor
     *
     * @param Context $context
     * @param DateTime $dateTime
     * @param Encrypted $encrypted
     * @param Session $customerSession
     * @param ObjectManagerInterface $appInterface
     * @param PaymentHelper $paymentHelper
     * @param Product $productModel
     */
    public function __construct(
        Context $context,
        DateTime $dateTime,
        Encrypted $encrypted,
        Session $customerSession,
        ObjectManagerInterface $appInterface,
        PaymentHelper $paymentHelper,
        Product $productModel
    ) {
        $this->_logger = $context->getLogger();
        $this->_scopeConfig = $context->getScopeConfig();
        $this->_dateTime = $dateTime;
        $this->_encrypted = $encrypted;
        $this->_customerSession = $customerSession;
        $this->_appInterface = $appInterface;
        $this->_paymentHelper = $paymentHelper;
        $this->_productModel = $productModel;

        parent::__construct($context);
    }

    /**
     * Get authorization details for payment method
     *
     * @param \Magento\Payment\Model\MethodInterface $paymentMethodInstance payment method instance
     * @return array
     */
    public function getAuthorization(\Magento\Payment\Model\MethodInterface $paymentMethodInstance)
    {
        $storeId = $paymentMethodInstance->getStore();

        switch ($this->getModus($paymentMethodInstance)) {
            case 'live':
                $authorizaion = [
                    'merchantid' => $paymentMethodInstance->getConfigData(
                        self::XML_FIELD_PROD_MODE_MERCHANT_ID,
                        $storeId
                    ),
                    'portfolioid' => $paymentMethodInstance->getConfigData(
                        self::XML_FIELD_PROD_MODE_PORTFOLIO_ID,
                        $storeId
                    ),
                    'password' => $this->_encrypted->processValue(
                        $paymentMethodInstance->getConfigData(
                            self::XML_FIELD_PROD_MODE_PASSWORD,
                            $storeId
                        )
                    )
                ];
                break;
            case 'test':
                $authorizaion = [
                    'merchantid' => $paymentMethodInstance->getConfigData(
                        self::XML_FIELD_TEST_MODE_MERCHANT_ID,
                        $storeId
                    ),
                    'portfolioid' => $paymentMethodInstance->getConfigData(
                        self::XML_FIELD_TEST_MODE_PORTFOLIO_ID,
                        $storeId
                    ),
                    'password' => $this->_encrypted->processValue(
                        $paymentMethodInstance->getConfigData(
                            self::XML_FIELD_TEST_MODE_PASSWORD,
                            $storeId
                        )
                    )
                ];
                break;
            default:
                $authorizaion = [
                    'merchantid' => '',
                    'portfolioid' => '',
                    'password' => ''
                ];
                break;
        }

        return $authorizaion;
    }

    /**
     * Get payment mode for payment method
     *
     * @param \Magento\Payment\Model\MethodInterface $paymentMethodInstance payment method instance
     * @return string
     */
    public function getModus(\Magento\Payment\Model\MethodInterface $paymentMethodInstance)
    {
        return $paymentMethodInstance->getConfigData(
            self::XML_FIELD_TEST_MODE_ACTIVE,
            $paymentMethodInstance->getStore()
        ) ? 'test' : 'live';
    }

    /**
     * Get appropriate Afterpay VAT category for a given Magento product tax class ID or string (for discount/shipping)
     *
     * @param \Magento\Payment\Model\MethodInterface $paymentMethodInstance payment method instance
     * @param string|integer $identifier VAT category
     * @return integer
     */
    public function getAfterpayVATCategory(\Magento\Payment\Model\MethodInterface $paymentMethodInstance, $identifier)
    {
        if (!count($this->_VATSetup)) {
            $this->setAfterpayVATCategories($paymentMethodInstance);
        }
        if (isset($this->_VATSetup[$identifier])) {
            return $this->_VATSetup[$identifier];
        } else {
            return $this->_VATSetup['fallback'];
        }
    }

    /**
     * Retrieve VAT setup from default payment method configuration. Create an array in the form of
     * [
     *     ['Tax class identifier OR product tax class ID' => 'Afterpay VAT Category ID']
     * ]
     *
     * @param \Magento\Payment\Model\MethodInterface $paymentMethodInstance payment method object
     */
    public function setAfterpayVATCategories(\Magento\Payment\Model\MethodInterface $paymentMethodInstance)
    {
        $this->_VATSetup = [];
        $storeId = $paymentMethodInstance->getStore();
        $defaultPaymentMethod = $this->_appInterface->create(self::DEFAULT_PAYMENT_METHOD_MODEL);
        $defaultPaymentMethod->setStore($storeId);
        // First set the fallback category
        $this->_VATSetup['fallback'] = $defaultPaymentMethod->getConfigData(
            self::XML_FIELD_VAT_CONFIG_FALLBACK,
            $storeId
        );
        // Then go through rest of config, if set, add to config array
        if ($id = $defaultPaymentMethod->getConfigData(self::XML_FIELD_VAT_CONFIG_SHIPPING, $storeId)) {
            $this->_VATSetup['shipping'] = $id;
        }
        if ($id = $defaultPaymentMethod->getConfigData(self::XML_FIELD_VAT_CONFIG_DISCOUNT, $storeId)) {
            $this->_VATSetup['discount'] = $id;
        }
        if ($id = $defaultPaymentMethod->getConfigData(self::XML_FIELD_VAT_CONFIG_FEE, $storeId)) {
            $this->_VATSetup['fee'] = $id;
        }
        // Product tax classes are configured as multiselect
        $productTaxClasses = [
            self::XML_FIELD_VAT_CONFIG_1 => 1,
            self::XML_FIELD_VAT_CONFIG_2 => 2,
            self::XML_FIELD_VAT_CONFIG_3 => 3,
            self::XML_FIELD_VAT_CONFIG_4 => 4,
            self::XML_FIELD_VAT_CONFIG_5 => 5
        ];
        foreach ($productTaxClasses as $configId => $afterpayId) {
            if ($ids = $defaultPaymentMethod->getConfigData($configId, $storeId)) {
                foreach (explode(',', $ids) as $id) {
                    $this->_VATSetup[$id] = $afterpayId;
                }
            }
        }
    }

    /**
     * Get customer initials
     *
     * @param string $firstName first name
     * @return string
     */
    public function getInitials($firstName)
    {
        $nameParts = explode(' ', trim($firstName));
        $initials = '';

        foreach ($nameParts as $namePart) {
            $initials .= ucfirst($namePart[0]) . '.';
        }

        return $initials;
    }

    /**
     * Format date of birth for request
     *
     * @param string $dob date of birth
     * @return string
     */
    public function formatDob($dob)
    {
        return (string)str_replace(' ', 'T', $this->_dateTime->formatDate(new \DateTime($dob), true));
    }

    /**
     * Get ISO language depending on payment method
     *
     * @param string $paymentMethod payment method code
     * @return string
     */
    public function getIsoLanguage($paymentMethod)
    {
        return $this->_isoLanguages[$paymentMethod];
    }

    /**
     * Get logger
     *
     * @return \Psr\Log\LoggerInterface
     */
    public function getLogger()
    {
        return $this->_logger;
    }

    /**
     * Get customer group id
     *
     * @return int
     */
    public function getCustomerGroupId()
    {
        return $this->_customerSession->getCustomerGroupId();
    }

    /**
     * Check if group is allowed
     *
     * @param $groupIds
     * @return bool
     */
    public function isGroupAllowed($groupIds)
    {
        $customerGroupId = $this->getCustomerGroupId();
        $allowedGroupIds = explode(",", $groupIds);

        if (in_array($customerGroupId, $allowedGroupIds)) {
            return true;
        }

        return false;
    }

    /**
     * Returns decrypted push notification password for provided payment method
     *
     * @param $paymentMethodCode
     * @return string
     */
    public function getPushPassword($paymentMethodCode)
    {
        return $this->_encrypted->processValue(
            $this->_scopeConfig->getValue('payment/' . $paymentMethodCode . '/' . self::XML_FIELD_TEST_MODE_ACTIVE) ?
                $this->_scopeConfig->getValue('payment/' . $paymentMethodCode . '/' . self::XML_FIELD_TEST_MODE_PUSH_PASSWORD) :
                $this->_scopeConfig->getValue('payment/' . $paymentMethodCode . '/' . self::XML_FIELD_PROD_MODE_PUSH_PASSWORD)
        );
    }

    /**
     * @return array
     */
    public function getAllowedMethods()
    {
        $result = [];
        $activeMethods = $this->_paymentHelper->getPaymentMethodList();

        if (!empty($activeMethods)) {
            foreach (array_keys($activeMethods) as $activeMethod) {
                if (strpos($activeMethod, 'afterpay_') !== false) {
                    $result[] = $activeMethod;
                }
            }
        }
        return $result;
    }

    /**
     * @return mixed
     */
    public function isCaptureEnabled()
    {
        return $this->_scopeConfig->getValue(self::PAYMENT_CAPTURE . 'active');
    }


    /**
     * @return mixed
     */
    public function orderCaptureAccepted()
    {
        return $this->_scopeConfig->getValue(self::PAYMENT_CAPTURE . 'order_status_accepted');
    }

    /**
     * @return mixed
     */
    public function orderCaptureRefused()
    {
        return $this->_scopeConfig->getValue(self::PAYMENT_CAPTURE . 'order_status_refused');
    }

    /**
     * @return mixed
     */
    public function isRefundEnabled()
    {
        return $this->scopeConfig->getValue(self::XML_FIELD_REFUND_ENABLED);
    }

    /**
     * @param $productId
     * @return mixed
     */
    public function getTaxClassId($productId)
    {
        $this->_productModel->load($productId);

        return $this->_productModel->getTaxClassId();
    }
}
