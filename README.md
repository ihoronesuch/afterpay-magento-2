# AfterPay module for Magento 2 #

The official Magento 2 module for the AfterPay payment method. This module offers a direct connection with the AfterPay payment service.

Current version: 1.3.9

## Installation ##

### Step 1. Check permissions ###

Before module installation, make sure that magento2 is installed on your web server.
The web server user must have write access to the following files and directories:

* var
* app/etc
* pub

### Step 2. Run composer require and update ###

Open terminal, change dir to project root/ folder, and run the following command to install and update the AfterPay module and dependencies:

```
composer require afterpay/afterpay-module
```

### Step 4. Redeploy static content, update Magento and renew cache ###

Open terminal, change dir to project root/ folder, and run following commands, which will move all contents from vendor to pub/static folder:

* 'sudo rm -R pub/static' - this command will delete pub/static folder, this is required in order to update static files
* 'sudo php bin/magento setup:static-content:deploy' - this command will move all vendor files to appropriate pub/static folders
* 'sudo php bin/magento setup:upgrade' (if you run into permissions error, change permissions for appropriate folders and rerun command) - this command will run all install/upgrade scripts
* 'sudo php bin/magento cache:flush' - this command will flush all caches

At this point module should be installed, and you can proceed with module configuration.

For more information please contact your business consultant at AfterPay: support@afterpay.nl

## Release notes ##

**1.3.9**
* Added AfterPay Library version 1.2.6 as requirement in composer

**1.3.8**
* Removed composer.lock to make sure latest library is being used

**1.3.6**
* Fixed problem with upgradedata not using EavSetupFactory

**1.3.6**
* Changed required PHP version because of new requirements Magento 2
* Fixed problem with javascript bug in checkout for the terms and conditions
* Set terms and conditions to enable in default configuration
* Updated some texts in default configuration
* Renamed payment methods to default structure

**1.3.4**
* (APS-49) Fixed problem with javascript bug in checkout: always requesting check for terms and conditions
* (APS-47) Added preferences for PaymentInformationManagement to allow custom error messages

**1.3.3**
* Fixed problem with missing jQuery dependency

**1.3.2**
* Updated version number for packagist automatic push test

**1.3.1**
* (APS-43) Changed scope of multiple configuration fields.

**1.3.0**

* Fixed bug with multiple payment methods not being validated
* (APS-40) Custom FE validation for terms and conditions
* (APS-39) Update files to use LocalizedExceptions instead of PaymentExceptions, so that they can be catched in next Magento versions
* (APS-42) Fix for wrong shipping refund value
* Updated to latest version of AfterPay Class
* Fixed bug which caused compilation errors
* (APS-41) Fix for bundled products with fixed price

**1.2.2**
* Removed $moduleResource from contruction because of compilation failures

**1.2.1**
* Removed vendor folder, so that it can be loaded by composer
* Added version number

**1.2.0**
* Bugfix issue with Zend Logger replaced by Monolog 

**1.1.2**
* Fixed issue with comments in CSS
* Updated AfterPay Library

**1.1.1**
* Fixed problem with loading version number in Admin

**1.1.0**
* New payment methods (Direct Debit, B2B, Belgium) and order management

**1.0.3**
* New version number and instructions

**1.0.2**
* New version number and instructions

**1.0.1**
* New version number and description

**1.0.0-dev**
* First development version, only NL digital invoice