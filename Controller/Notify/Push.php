<?php
/**
 * Copyright (c) 2016  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2016 arvato Finance B.V.
 */
namespace Afterpay\Payment\Controller\Notify;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\OrderFactory;
use Afterpay\Payment\Helper\Service\Data as ServiceHelper;

class Push extends Action
{
    /**
     * Order factory
     *
     * @var OrderFactory
     */
    protected $_orderFactory;

    /**
     * Service helper
     *
     * @var ServiceHelper
     */
    protected $_serviceHelper;

    /**
     * Callback constructor.
     * @param Context        $context
     * @param OrderFactory   $orderFactory
     * @param ServiceHelper  $serviceHelper
     */
    public function __construct(Context $context, OrderFactory $orderFactory, ServiceHelper $serviceHelper)
    {
        $this->_orderFactory = $orderFactory;
        $this->_serviceHelper = $serviceHelper;
        parent::__construct($context);
    }

    /**
     *
     * Parameters available:
     *
     * merchantId Merchant ID of the client
     * portefeuilleId Portefeuille ID that has been used
     * orderReference Order reference (order number or parentTransactionReference) that was provided during authorization.
     * signature MD5 hash on merchantId + portfolioId + Password + orderReference + statusCode
     * statusCode A /W/ P/V
     * timestampOut Unix timestamp of the sent request.
     * transactionId AfterPay transaction ID
     *
     *
     * @return \Magento\Framework\App\Response\Http|\Magento\Framework\App\ResponseInterface
     */
    public function execute()
    {
        $order = $this->_orderFactory->create();
        $order->loadByIncrementId($this->getRequest()->getParam('orderReference'));

        // Checking if order was found
        if (!$order->getId()) {
            /** @var \Magento\Framework\App\Response\Http $response */
            $response = $this->getResponse();
            $response->setStatusCode(404);
            $response->setBody('Order not found');
            return $response;
        }

        // Validating signature
        if (!$this->validateSignature($order)) {
            /** @var \Magento\Framework\App\Response\Http $response */
            $response = $this->getResponse();
            $response->setStatusCode(400);
            $response->setBody('Invalid signature');
            return $response;
        }

        // Performing action
        switch ($this->getRequest()->getParam('statusCode')) {
            case 'A': // Accept
                /** @var Order\Payment $payment */
                $payment = $order->getPayment();
                $order->setStatus($payment->getMethodInstance()->getConfigData('pending_order_status'));
                $payment->registerCaptureNotification($order->getBaseGrandTotal());
                $order->setTotalPaid($order->getBaseGrandTotal());
                $order->save();
                break;
            case 'W': // Reject
            case 'V':
                $order->cancel()->save();
                /** @var \Magento\Sales\Model\Order\Status\History $comment */
                $comment = $order->addStatusHistoryComment(__('Order canceled by reject notification'));
                $comment->save();
                break;
            case 'P': // Still pending, add comment
                if ($this->getRequest()->getParam('subStatusCode')) {
                    /** @var \Magento\Sales\Model\Order\Status\History $comment */
                    $comment = $order->addStatusHistoryComment($this->getRequest()->getParam('subStatusCode'));
                    $comment->save();
                }
                break;
            default: // Fallback
                /** @var \Magento\Framework\App\Response\Http $response */
                $response = $this->getResponse();
                $response->setStatusCode(400);
                $response->setBody('Invalid status code');
                return $response;
                break;
        }

        // Empty response with 200 code
        return $this->getResponse();
    }

    /**
     * @param Order $order
     * @return bool
     */
    protected function validateSignature(Order $order)
    {
        $signature = md5(
            $this->getRequest()->getParam('merchantId') .
            $this->getRequest()->getParam('portefeuilleId') .
            $this->_serviceHelper->getPushPassword($order->getPayment()->getMethod()) .
            $this->getRequest()->getParam('orderReference') .
            $this->getRequest()->getParam('statusCode')
        );
        return $signature === $this->getRequest()->getParam('signature');
    }
}
