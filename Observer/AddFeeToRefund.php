<?php
/**
 * Copyright (c) 2016  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2016 arvato Finance B.V.
 */
namespace Afterpay\Payment\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\ManagerInterface;
use Afterpay\Payment\Model\Method\Factories;
use \Magento\Framework\App\Request\Http as Request;

/**
 * Add payment fee to payment
 */
class AddFeeToRefund implements ObserverInterface
{

    const REFUND_AFTERPAY_SERVICE_FEE = 'refund_afterpay_fee';

    /**
     * @var ManagerInterface
     */
    protected $_eventManager;

    /**
     * AfterPay payment methods factories
     *
     * @var Factories
     */
    protected $_paymentMethodFactories;

    /**
     * @var Request
     */
    protected $_request;
    
    /**
     * @param ManagerInterface $eventManager
     * @param Factories $paymentMethodFactories
     * @param Request $request
     * @throws \Exception
     */
    public function __construct(
        ManagerInterface $eventManager,
        Factories $paymentMethodFactories,
        Request $request
    ) {
        $this->_eventManager = $eventManager;
        $this->_paymentMethodFactories = $paymentMethodFactories;
        $this->_request = $request;
    }

    /**
     * Add payment fee to payment
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $creditmemoParams = $this->_request->getParam('creditmemo');
        $creditmemo = $observer->getCreditmemo();
        if (isset($creditmemoParams[self::REFUND_AFTERPAY_SERVICE_FEE])
            && $creditmemoParams[self::REFUND_AFTERPAY_SERVICE_FEE] == 1
        ) {
            $creditmemo->setAfterpayServiceTaxRefund(1);
            $order = $creditmemo->getOrder();
            
            $serviceFeeAmount = $order->getAfterpayPaymentFee();
            $baseServiceFeeAmount = $order->getBaseAfterpayPaymentFee();
            
            $grandTotal = $creditmemo->getGrandTotal();
            $creditmemo->setGrandTotal($serviceFeeAmount + $grandTotal);
            
            $baseGrandTotal = $creditmemo->getBaseGrandTotal();
            $creditmemo->setBaseGrandTotal($baseServiceFeeAmount + $baseGrandTotal);
            
            $adjustmentPositive = $creditmemo->getAdjustmentPositive();
            $creditmemo->setAdjustmentPositive($adjustmentPositive + $serviceFeeAmount);
            
            $adjustmentNegative = $creditmemo->getAdjustmentNegative();
            $creditmemo->setAdjustment($adjustmentPositive + $serviceFeeAmount - $adjustmentNegative);
        }
    }
}
