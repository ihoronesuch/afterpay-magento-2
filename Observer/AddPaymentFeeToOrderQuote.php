<?php
/**
 * Copyright (c) 2016  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2016 arvato Finance B.V.
 */
namespace Afterpay\Payment\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\ManagerInterface;
use Afterpay\Payment\Model\Method\Factories;
use Magento\Framework\App\State;

/**
 * Add payment fee to order quote
 */
class AddPaymentFeeToOrderQuote implements ObserverInterface
{
    /**
     * @var ManagerInterface
     */
    protected $_eventManager;

    /**
     * Application state
     *
     * @var State
     */
    protected $_isBackend;

    /**
     * AfterPay payment methods factories
     *
     * @var Factories
     */
    protected $_paymentMethodFactories;

    /**
     * @param ManagerInterface $eventManager
     * @param State $appState
     * @param Factories $paymentMethodFactories
     */
    public function __construct(
        ManagerInterface $eventManager,
        State $appState,
        Factories $paymentMethodFactories
    ) {
        $this->_eventManager = $eventManager;
        $this->_isBackend = $appState->getAreaCode() == \Magento\Framework\App\Area::AREA_ADMINHTML;
        $this->_paymentMethodFactories = $paymentMethodFactories;
    }

    /**
     * Add payment fee to quote and order
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var \Magento\Framework\Event $event */
        $event = $observer->getEvent();
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $observer->getEvent()->getQuote();
        /** @var \Magento\Sales\Model\Order $order */
        $order = $event->getOrder();
        /** @var \Magento\Sales\Model\Order\Payment $payment */
        $payment = $order->getPayment();
        $paymentMethod = $payment->getMethod();

        /**
         * Add payment fee only for AfterPay methods
         */
        if ($this->_paymentMethodFactories->paymentMethodExists($paymentMethod)) {
            $paymentMethodInstance = $this->_paymentMethodFactories->getPaymentMethodInstance($paymentMethod);
            $orderPaymentFeeAmount = $paymentMethodInstance->getFeeValue();
            $baseOrderPaymentFeeAmount = $orderPaymentFeeAmount;

            $quote->setAfterpayPaymentFee($orderPaymentFeeAmount);
            $quote->setBaseAfterpayPaymentFee($baseOrderPaymentFeeAmount);

            $order->setAfterpayPaymentFee($orderPaymentFeeAmount);
            $order->setBaseAfterpayPaymentFee($baseOrderPaymentFeeAmount);

            if (!$this->_isBackend) {
                $quote->setGrandTotal($quote->getGrandTotal() + $orderPaymentFeeAmount);
                $quote->setBaseGrandTotal($quote->getBaseGrandTotal() + $baseOrderPaymentFeeAmount);

                $order->setGrandTotal($order->getGrandTotal() + $orderPaymentFeeAmount);
                $order->setBaseGrandTotal($order->getBaseGrandTotal() + $baseOrderPaymentFeeAmount);
            }

        }

        return $this;
    }
}
