<?php
/**
 * Copyright (c) 2016  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2016 arvato Finance B.V.
 */
namespace Afterpay\Payment\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\ObjectManagerInterface;

/**
 * Update customer information with AfterPay-required fields possibly entered while submitting order so that customer
 * input can be reused for future purchases.
 */
class UpdateCustomerInfo implements ObserverInterface
{

    private $_appInterface;
    public function __construct(ObjectManagerInterface $objectManagerInterface){
        $this->_appInterface = $objectManagerInterface;
    }
    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var \Magento\Sales\Model\Order $order */
        $order = $observer->getEvent()->getOrder();

        if ($order->getCustomerId()) {
            $customer = $this->_appInterface->create('\Magento\Customer\Model\Customer')->load($order->getCustomerId());
            if($customer->getId()){
                $dob = $order->getCustomerDob();
                if ($dob && $customer->getDob() !== $dob) {
                    $customer->setDob($dob);
                }

                $gender = $observer->getEvent()->getGender();
                if ($gender && $gender !== $customer->getGender()) {
                    $customer->setGender($gender);
                }

                $vat = $order->getCustomerTaxvat();
                if ($vat && $customer->getTaxvat() !== $vat) {
                    $customer->setTaxvat($vat);
                }

                $coc = $order->getCustomerCocNumber();
                if ($coc && $customer->getData('cocnumber') !== $coc) {
                    $customer->setCocnumber($coc);
                }

                $customer->save();

                $company = $order->getCustomerCompany();
                if ($company && ($order->getShippingAddress() || $order->getBillingAddress())) {
                    $customerAddressId = $order->getShippingAddress() ?
                        $order->getShippingAddress()->getCustomerAddressId() :
                        $order->getBillingAddress()->getCustomerAddressId();
                    /** @var \Magento\Customer\Model\Address $customerAddress */
                    $customerAddress = $this->_appInterface->create('\Magento\Customer\Model\Address')
                        ->load($customerAddressId);
                    if ($customerAddress->getId() && !$customerAddress->getCompany()) {
                        $customerAddress->setCompany($company);
                        $customerAddress->save();
                    }
                }
            }
        };

        return $this;
    }
}
