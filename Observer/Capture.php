<?php
/**
 * Copyright (c) 2016  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2016 arvato Finance B.V.
 */


namespace Afterpay\Payment\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\State;
use Afterpay\Afterpay as Afterpay;
use Magento\Checkout\Model\Session as CheckoutSession;
use Afterpay\Payment\Helper\Service\DataFactory as ServiceHelperFactory;
use Afterpay\Payment\Helper\Debug\Data as DebugHelper;

/**
 * Class Capture
 * @package Afterpay\Payment\Observer
 *
 */
class Capture implements ObserverInterface
{

    const CAPTURE_RESULT_SUCCESS = '0';

    const CAPTURE_RESULT_REFUSED = '3';

    const ORDER_MANAGEMENT_CODE = 'OM';

    /**
     * Service model
     */
    protected $_afterpayService;

    /**
     * Payment method inctance
     */
    protected $_methodInstance;

    /**
     * Helper
     *
     * @var \Afterpay\Payment\Helper\Service\Data
     */
    protected $_helper;

    /**
     * Checkout session
     *
     * @var
     */
    protected $_session;

    /**
     * Debug
     *
     * @var
     */
    protected $_debugHelper;


    /**
     * Capture constructor.
     * @param Afterpay $afterpayService
     * @param ServiceHelperFactory $serviceHelperFactory
     * @param CheckoutSession $checkoutSession
     * @param DebugHelper $debugHelper
     */
    public function __construct(
        Afterpay $afterpayService,
        ServiceHelperFactory $serviceHelperFactory,
        CheckoutSession $checkoutSession,
        DebugHelper $debugHelper
    ) {
        $this->_afterpayService = $afterpayService;
        $this->_helper = $serviceHelperFactory->create();
        $this->_session = $checkoutSession;
        $this->_debugHelper = $debugHelper;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return bool
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $allowedMethods = $this->_helper->getAllowedMethods();
        /** @var  $invoice \Magento\Sales\Model\Order\Invoice */
        $invoice = $observer->getInvoice();
        /** @var  $order \Magento\Sales\Model\Order */
        $order = $observer->getDataObject()->getOrder();

        $this->_methodInstance = $order->getPayment()->getMethodInstance();

        if (!in_array($this->_methodInstance->getCode(), $allowedMethods)
            || !$this->_helper->isCaptureEnabled()
            || $this->_session->getQuote()->getAfterPayCaptureStarted()
            || $order->getAfterpayCaptured()
            || $invoice->getIsUsedForRefund()
        ) {
            return false;
        }
        $address = $order->getBillingAddress();
        $countryId = $address->getCountryId();
        $this->_afterpayService->set_ordermanagement('capture_full');
        $this->_afterpayService->country = $countryId;
        $apOrder = [];
        $apOrder['invoicenumber'] = $invoice->getIncrementId();
        $apOrder['ordernumber'] = $order->getIncrementId();

        $this->_afterpayService->set_order($apOrder, self::ORDER_MANAGEMENT_CODE);

        $this->_afterpayService->do_request(
            $this->_helper->getAuthorization($this->_methodInstance),
            $this->_helper->getModus($this->_methodInstance)
        );
        $response = $this->_afterpayService->order_result->return;
        $this->_setOrderStatus($response, $order, $invoice);

        return true;
    }

    /**
     * @param $response
     * @param $order \Magento\Sales\Model\Order
     * @param $invoice \Magento\Sales\Model\Order\Invoice
     */
    protected function _setOrderStatus($response, $order, $invoice)
    {
        $quote = $this->_session->getQuote();
        if (!$quote->getAfterPayCaptureStarted()) {
            $quote->setAfterPayCaptureStarted(1);
            switch ($response->resultId) {
                case self::CAPTURE_RESULT_SUCCESS:
                    $order->addStatusHistoryComment(__('This order has been captured by AfterPay'))->save();
                    $invoice->setTransactionId($response->transactionId)->save();
                    $order->setStatus($this->_helper->orderCaptureAccepted());
                    $order->setAfterpayCaptured(1);
                    break;
                default:
                    $order->addStatusHistoryComment(__('AfterPay capture attempt has failed'))->save();
                    $invoice->cancel()->save();
                    $order->setStatus($this->_helper->orderCaptureRefused());
                    $this->_debugHelper->debug($order->getPayment()->getMethod(), array($response));
            }
            $order->save();
        }
    }
}